package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

public class DuplicateFinderTest {

    private DuplicateFinder duplicateFinder = new DuplicateFinder();

    @Test(expected = IllegalArgumentException.class)
    public void test() {
        //run
        duplicateFinder.process(null, new File("a.txt"));

        //assert : exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1() {
        //run
        duplicateFinder.process(new File("a.txt"), null);

        //assert : exception
    }

    @Test
    public void test2() {
        //given
        File sourceFile = new File("a.txt");
        File targetFile = new File("b.txt");

        //run
        boolean result = duplicateFinder.process(sourceFile, targetFile);

        //assert
        Assert.assertFalse(result);
        Assert.assertFalse(targetFile.exists());
    }

    @Test
    public void test3() {
        //given
        String inputString = "ccc" + System.lineSeparator() +
                "ddd" + System.lineSeparator() +
                "bbb" + System.lineSeparator() +
                "ddd" + System.lineSeparator() +
                "ddd" + System.lineSeparator() +
                "aaa";
        String outputString = "aaa[1]" + System.lineSeparator() +
                "bbb[1]" + System.lineSeparator() +
                "ccc[1]" + System.lineSeparator() +
                "ddd[3]" + System.lineSeparator();
        File sourceFile = createFile(inputString);
        File targetFile = createFile("");

        //run
        boolean result = duplicateFinder.process(sourceFile, targetFile);

        //assert
        Assert.assertTrue(result);
        Assert.assertTrue(testFile(targetFile, outputString));
    }

    @Test
    public void test4() {
        //given
        String inputString = "ccc" + System.lineSeparator() +
                "ddd" + System.lineSeparator() +
                "bbb" + System.lineSeparator() +
                "ddd" + System.lineSeparator() +
                "ddd" + System.lineSeparator() +
                "aaa";
        String outputString = "bla-bla-bla" + System.lineSeparator() +
                "aaa[1]" + System.lineSeparator() +
                "bbb[1]" + System.lineSeparator() +
                "ccc[1]" + System.lineSeparator() +
                "ddd[3]" + System.lineSeparator();
        File sourceFile = createFile(inputString);
        File targetFile = createFile("bla-bla-bla" + System.lineSeparator());

        //run
        boolean result = duplicateFinder.process(sourceFile, targetFile);

        //assert
        Assert.assertTrue(result);
        Assert.assertTrue(testFile(targetFile, outputString));
    }

    private File createFile(String content) {
        try {
            File file = File.createTempFile("dft", null);
            file.deleteOnExit();
            try (FileWriter writer = new FileWriter(file)) {
                writer.write(content);
            }
            return file;
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    private boolean testFile(File file, String content) {
        if (file == null || content == null) return false;
        try {
            FileInputStream fis = new FileInputStream(file);
            byte[] str = new byte[fis.available()];
            fis.read(str);
            String result = new String(str);
            return result.equals(content);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
}