package com.tsystems.javaschool.tasks.calculator;


import java.text.NumberFormat;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.length() == 0 || !statement.matches("^(\\d|\\+|-|\\*|/|\\(|\\)|\\.|\\s)+$")) {
            return null;
        }
        String statementInRPN = convertFromInfixNotationToRPN(statement);
        return calculateRPN(statementInRPN);
    }

    private String convertFromInfixNotationToRPN(String infixNotation) {
        StringTokenizer tokenizer = new StringTokenizer(infixNotation, "+-*/() ", true);
        Deque<String> tempStack = new ArrayDeque<>();
        StringJoiner result = new StringJoiner(" ");

        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (token.matches("^\\s$")) continue;

            if (token.matches("^\\d*\\.?\\d*$")) {
                if (token.matches("^\\d+\\.$")) result.add(token + "0");
                else if (token.matches("^\\.\\d+$")) result.add("0" + token);
                else if (token.matches("^\\d+\\.?\\d*$")) result.add(token);
            }

            if (token.equals("(")) tempStack.addFirst(token);

            if (token.equals(")")) {
                try {
                    String str;
                    while (!(str = tempStack.removeFirst()).equals("(")) {
                        result.add(str);
                    }
                } catch (NoSuchElementException e) {
                    return null;
                }
            }

            if (token.matches("^\\+|-|\\*|/$")) {
                while (isPriorityOfOperatorLessOrEqual(token, tempStack.peekFirst())) {
                    result.add(tempStack.removeFirst());
                }
                tempStack.addFirst(token);
            }

        }

        while (!tempStack.isEmpty()) {
            String str = tempStack.pollFirst();
            if (str.matches("^\\+|-|\\*|/$")) {
                result.add(str);
            } else {
                return null;
            }
        }

        return result.toString();
    }

    private boolean isPriorityOfOperatorLessOrEqual(String firstOperator, String secondOperator) {
        if (firstOperator == null || secondOperator == null) return false;
        switch (firstOperator) {
            case "+":
            case "-":
                if (secondOperator.matches("^\\+|-|\\*|/$")) return true;
            case "*":
            case "/":
                if (secondOperator.matches("^\\+|-$")) return false;
                if (secondOperator.matches("^\\*|/$")) return true;
            default:
                return false;
        }
    }

    private String calculateRPN(String statementInRPN) {
        if (statementInRPN == null) return null;

        StringTokenizer tokenizer = new StringTokenizer(statementInRPN, "+-*/ ", true);
        Deque<Double> evaluationStack = new ArrayDeque<>();

        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            if (token.matches("^\\s$")) continue;

            if (token.matches("^\\d*\\.?\\d*$")) {
                try {
                    evaluationStack.addFirst(Double.parseDouble(token));
                } catch (NumberFormatException e) {
                    return null;
                }
            }

            if (token.matches("^\\+|-|\\*|/$")) {
                try {
                    Double op2 = evaluationStack.pollFirst();
                    Double op1 = evaluationStack.pollFirst();
                    Double result = calculate(op1, op2, token);
                    evaluationStack.addFirst(result);
                } catch (IllegalArgumentException | ArithmeticException e) {
                    return null;
                }
            }
        }

        Double result = evaluationStack.pollFirst();
        NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
        nf.setMaximumFractionDigits(4);
        nf.setGroupingUsed(false);
        
        return nf.format(result);
    }

    private Double calculate(Double op1, Double op2, String operator) {
        if (op1 == null || op2 == null || operator == null || !operator.matches("^\\+|-|\\*|/$"))
            throw new IllegalArgumentException();

        Double result;
        switch (operator) {
            case "+":
                result = op1 + op2;
                break;
            case "-":
                result = op1 - op2;
                break;
            case "*":
                result = op1 * op2;
                break;
            case "/":
                result = op1 / op2;
                break;
            default:
                throw new IllegalArgumentException();
        }

        if (result.isInfinite() || result.isNaN()) throw new ArithmeticException();
        return result;
    }
}