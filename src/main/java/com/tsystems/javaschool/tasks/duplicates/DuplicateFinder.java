package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        if (sourceFile == null || targetFile == null) throw new IllegalArgumentException();
        if (!sourceFile.exists()) return false;

        try (BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
             BufferedWriter writer = new BufferedWriter(new FileWriter(targetFile, true))) {

            TreeMap<String, Integer> lines = new TreeMap<>();

            while (reader.ready()) {
                String line = reader.readLine();
                lines.put(line, lines.containsKey(line) ? lines.get(line) + 1 : 1);
            }

            for (Map.Entry<String, Integer> entry : lines.entrySet()) {
                writer.write(String.join("", entry.getKey(), "[", Integer.toString(entry.getValue()), "]"));
                writer.newLine();
            }

        } catch (IOException e) {
            return false;
        }

        return true;
    }
}